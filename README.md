# Vagrant setup for testing and provisioning Yubikeys

This Vagrant environment sets up a Debian 10 virtual machine with tools and libraries for handling Yubikey security token.

## Prerequirements

* [Vagrant](https://www.vagrantup.com/downloads)
* [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-with-pip)

## Installation

Clone this repo:

```
mkdir vagrant
cd vagrant
git clone https://gitlab.mis.mpg.de/vagrant/yubikey.git
```

## Setup

Clone the needed ansible-role via ansible-galaxy

```
cd yubikey
ansible-galaxy install -r requirements.yml
```

## Startup

Start your virtual environment

```
vagrant up
```

## Login

Log in to the virtual machine

```
vagrant ssh
```

## Destroy

Destroy all traces

```
vagrant destroy
```
